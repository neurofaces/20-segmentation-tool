function labeler()

addpath('F:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_functions\_fourier_descriptors');

% some parameters
folder_imgs = 'F:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd\__eyes_cropped_236x126\';
folder_gt = fullfile(folder_imgs, 'gt');
if(~exist(folder_gt, 'dir')), mkdir(folder_gt); end;
extension = '.bmp';

% list all 'extension' files in 'folder_imgs'
images = dir([folder_imgs '*' extension]);

% create figure and maximize
f = figure;
% add callback <== THIS DOES THE WHOLE JOB
set(f, 'KeyPressFcn', @(f,keydata) keypress_function(f,keydata));
% set(f, 'ButtonDownFcn', @(f, clickdata) mouseclick_function(f, clickdata));

% gather all necessary data
data = guidata(f);
data.f = f;
data.img_to_correct = 1;
data.folder_imgs = folder_imgs;
data.folder_gt = folder_gt;
data.extension = extension;
data.imgs = images;
data.n_imgs = numel(data.imgs);
guidata(f, data);
label_image(f);

% % ==================================================================>
% % FUNCTION THAT SHOW LANDMARKS AND ADD CALLBACK TO KEYPRESS
% % ==================================================================>
function label_image(f)

data = guidata(f);

% % Load image
img = imread(fullfile(data.folder_imgs, data.imgs(data.img_to_correct).name));

% show image
figure(f);
hold off;
cla;

try
    imshow(img);
    set(f,'units','normalized','outerposition',[0 0 1 1]); % Maximize figure.
    title(num2str(data.img_to_correct))
    
    h = imfreehand(gca);    
    setClosed(h, 1);
    % pos = wait(h);
    
    bw = h.createMask();
    bws = imfill(smooth_contours(bw, 20), 'holes');
    hold on, imshow(bws), alpha(.5)
    % imshowpair(data.img, bws);

    data.bws = bws;
    guidata(f, data);
catch
    
end

% % ==================================================================>
% % SAVE MASKS
% % ==================================================================>
function save_mask(f)

data = guidata(f);
imwrite(data.bws, fullfile(data.folder_gt, data.imgs(data.img_to_correct).name));
data.img_to_correct = data.img_to_correct + 1;
guidata(f, data);


% % ==================================================================>
% % CALLBACK FOR CLICK
% % ==================================================================>
% function mouseclick_function(f, clickdata)
% 
% data = guidata(f);



% % ==================================================================>
% % CALLBACK FOR KEYPRESS
% % ==================================================================>
function keypress_function(f,keydata)

data = guidata(f);

switch(keydata.Key)
    
    case 'c'
        label_image(f);
        return;
        
    case 's' % save grounbd truth and go to next image
        save_mask(f);
        
        label_image(f);
        return;
         
        
    case 'return'   % % save gt
        if(~ismember({'bws'}, fieldnames(data)))
            bw = h.createMask();
            bws = imfill(smooth_contours(bw, 20), 'holes');
            hold on, imshow(bws), alpha(.5)
            data.bws = bws;
            guidata(f, data);
        end
        return;
        
    case 'leftarrow'   % % Go to previous image (without saving)
        if(data.img_to_correct > 1)
            data.img_to_correct = data.img_to_correct - 1;
            guidata(f, data);
            label_image(f);
            return;
        end
        return; % if first image reached just return
        
    case 'rightarrow' % % Go to next image (without saving)
        if(data.img_to_correct < data.n_imgs)
            data.img_to_correct = data.img_to_correct + 1;
            guidata(f, data);
            label_image(f);
        else
            msgbox('All images corrected.');
        end
        return;
        
    case 'g' % go to image_to_correct #
        img_num = str2double(inputdlg('IMG to correct #: '));
        if(isempty(img_num)), return; end;
        if(img_num > 0 && img_num <= data.n_imgs)
            data.img_to_correct = img_num;
            data.point_to_correct = 1;
            guidata(f, data);
            label_image(f);
        else
            msgbox(['Number of images: ' num2str(data.n_imgs)]);
        end
        return;
        
    otherwise
        return;
end
